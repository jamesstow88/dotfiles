#!/bin/bash

if [ -e '/usr/bin/synclient' ]
then
    enable=$(synclient -l | grep -c 'TouchpadOff.*=.*0')
    if [ $enable == 1 ]; then
	echo '[ ]'
    else
	echo '[X]'
    fi
fi
