#!/bin/sh

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

: ${VERBOSE:=0}
: ${CP:=/bin/cp}
: ${LN:=/bin/ln}
: ${MKDIR:=/bin/mkdir}
: ${RM:=/bin/rm}
: ${DIRNAME:=/usr/bin/dirname}
: ${XARGS:=/usr/bin/xargs}

verbose() {
  if [ "$VERBOSE" -gt 0 ]; then
    echo "$@"
  fi
}

handle_file_cp() {
  if [ -e "$2" ]; then
    printf "%s " "overwrite $2? [yN]"
    read overwrite
    case "$overwrite" in
      y)
        $RM -rf "$2"
        ;;
      *)
        echo "skipping $2"
        return
        ;;
    esac
  fi
  verbose "'$1' -> '$2'"
  $DIRNAME "$2" | $XARGS $MKDIR -p
  $CP -R "$1" "$2"
}

handle_file_ln() {
  if [ -e "$2" ]; then
    printf "%s " "overwrite $2? [yN]"
    read overwrite
    case "$overwrite" in
      y)
        $RM -rf "$2"
        ;;
      *)
        echo "skipping $2"
        return
        ;;
    esac
  fi
  verbose "'$1' -> '$2'"
  $DIRNAME "$2" | $XARGS $MKDIR -p
  $LN -sf "$1" "$2"
}

mkdir -p "${HOME}/.bin"
mkdir -p "${HOME}/.config"
mkdir -p "${HOME}/.vim"

handle_file_ln "${HERE}/bin" "${HOME}/.bin"
handle_file_ln "${HERE}/config" "${HOME}/.config"
handle_file_ln "${HERE}/i3blocks.conf" "${HOME}/.i3blocks.conf"
handle_file_ln "${HERE}/compton.conf" "${HOME}/.compton.conf"
handle_file_ln "${HERE}/gitconfig" "${HOME}/.gitconfig"
# handle_file_ln "${HERE}/vim" "${HOME}/.vim"
# handle_file_ln "${HERE}/vimrc" "${HOME}/.vimrc"
handle_file_ln "${HERE}/xinitrc" "${HOME}/.xinitrc"
handle_file_ln "${HERE}/zshrc" "${HOME}/.zshrc"
