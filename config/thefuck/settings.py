# The Fuck settings file
#
# The rules are defined as in the example bellow:
#
# rules = ['cd_parent', 'git_push', 'python_command', 'sudo']
#
# The default values are as follows. Uncomment and change to fit your needs.
# See https://github.com/nvbn/thefuck#settings for more information.
#

# alter_history = True
# require_confirmation = True
# wait_command = 3
# history_limit = None
# debug = False
# rules = [<object object at 0x7fefebbb1140>]
# no_colors = False
# env = {'LC_ALL': 'C', 'LANG': 'C', 'GIT_TRACE': '1'}
# exclude_rules = []
# priority = {}
