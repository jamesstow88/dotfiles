export ZSH=/home/jim/.oh-my-zsh

ZSH_THEME="agnoster"
plugins=(git)

export PATH="/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl"

source $ZSH/oh-my-zsh.sh

conf() {
	case $1 in
		i3)		vim ~/.config/i3/config				;;
		i3blocks)	vim ~/.i3blocks.conf				;;
		zsh)		vim ~/.zshrc && zsh				;;
		*)		echo "'$1' is unknown"				;;
	esac
}

eval $(thefuck --alias) 

alias pacin='pacman -S'
alias pacrm='pacman -Rns'
alias pacup='pacman -Syu'
alias pacwat='(comm -23 <(pacman -Qqen|sort) <(pacman -Qqg base base-devel|sort)) | sort -n'
alias pacman='sudo pacman'

alias yup='yaourt -Syua'

alias cd..='cd ..'
alias fucking='sudo'
alias FUCKING='sudo'

alias gs='git st'
alias gps='git push'
alias gpl='git pull'

alias yaourt='yaourt --noconfirm'
